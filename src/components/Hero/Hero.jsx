import React from 'react'
import "./Hero.css"
import arrow from "../../assets/dark-arrow.png"
const Hero = () => {
  return (
    <div className='hero container'>
    <div className="hero-text">
    <h1>THE GREAT COLLEGE</h1>
      <h1>We Ensure better Education for better World</h1>
      <p>Our vission is to Empower Students with knowledge,skills and experience needed to excel in the world</p>
      <button className='btn'>Explore More <img src={arrow} alt="" /></button>
    </div>

    </div>
  )
}

export default Hero