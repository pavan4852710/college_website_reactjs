import React from 'react'
import "./Contact.css"

import msg_icon from "../../assets/msg-icon.png"
import mail_icon from "../../assets/mail-icon.png"
import phone_icon from "../../assets/phone-icon.png"
import location_icon from "../../assets/location-icon.png"
import white_arrow from "../../assets/white-arrow.png"

const Contact = () => {


    const [result, setResult] = React.useState("");

  const onSubmit = async (event) => {
    event.preventDefault();
    setResult("Sending....");
    const formData = new FormData(event.target);

    formData.append("access_key", "4dee32e9-a32c-4bd2-93a2-e729303313f1");

    const response = await fetch("https://api.web3forms.com/submit", {
      method: "POST",
      body: formData
    });

    const data = await response.json();

    if (data.success) {
      alert("Form Submitted Successfully")
      setResult("Form Submitted Successfully");
      event.target.reset();
    } else {
      console.log("Error", data);
      setResult(data.message);
    }
}



  return (
    <div className='contact'>
        <div className="contact-col">
            <h3>Send Us A Message <img src={msg_icon} alt="" /></h3>
            <p>Feel  free to reach out.
            Please Provide your valuable feedback.
            We will be improving.
            </p>

            <ul>
                <li><img src={mail_icon} alt="" />pavansai@gmail.com</li>
                <li><img src={phone_icon} alt="" />9999999999</li>
                <li><img src={location_icon} alt="" />pavan</li>
            </ul>
        </div>

        <div className="contact-col">
            <form onSubmit={onSubmit}>
                <label>Your Name</label>
                <input type="text" name='name' placeholder='enter name' required />
                
                <label>Phone Number</label>
                <input type="tel" name='phone' placeholder='enter phone' required />

                <label htmlFor="">Write your message</label>
                <textarea name="message" placeholder='enter message'  required rows="6" id=""></textarea>
                <button type='submit' className='btn dark-btn'>SUBMIT NOW <img src={white_arrow} alt="" /></button>
            </form>
            <span>{result}</span>
        </div>
    </div>
  )
}

export default Contact