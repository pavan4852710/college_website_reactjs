import React from 'react'
import "./About.css"
import about_img from "../../assets/about.png"
import play_icon from "../../assets/play-icon.png"



const About = ({setPlayState}) => {
  return (
    <div className='about'>
    <div className="about-left">

        <img className='about-img' src={about_img} alt="" />        
        <img onClick={()=>setPlayState(true)} className='play-icon' src={play_icon} alt="" />        

    </div>
        <div className="about-right">
            <h3>ABOUT UNIVERSITY</h3>
            <h2></h2>
            <p>Universities support a wide range of student organizations, 
            including academic clubs, cultural groups, and sports teams,
             providing opportunities for personal growth and community engagement.</p>
            <p>The rise of online education and digital learning tools
             is transforming traditional educational models, making higher 
             education more accessible globally.</p>
             <p>Modern universities offer extensive facilities such as
              libraries, laboratories, sports complexes, and student housing to support both academic and extracurricular activities.</p>
        </div>
        
    </div>
  )
}

export default About